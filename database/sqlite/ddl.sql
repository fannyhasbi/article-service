CREATE TABLE IF NOT EXISTS "ARTICLES" (
  "SLUG" TEXT,
  "NAME" TEXT,
  "BODY" TEXT
);