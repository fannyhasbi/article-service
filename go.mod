module gitlab.com/fannyhasbi/article-service

go 1.12

require (
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
)
