package main

import (
	"database/sql"
	"fmt"
	"gitlab.com/fannyhasbi/article-service/repository/sqlite"
	"io/ioutil"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
	"gitlab.com/fannyhasbi/article-service/repository/inmemory"
	"gitlab.com/fannyhasbi/article-service/storage"
)

func main() {
	var repo repository.ArticleRepository
	driver := "sqlite"

	switch driver {
	case "inmemory":
		repo = initInMemory()
	case "sqlite":
		repo = initSQLite()
	default:
		repo = initInMemory()
	}

	// article := model.Article{
	// 	Slug: "berita-baru",
	// 	Name: "Berita baru",
	// 	Body: "Body berita baru",
	// }

	// fmt.Println(article)
	// repo.Save(&article)
	// result := repo.FindBySlug("foobar")
	cat := model.Entertainment
	result := repo.GetPublishedArticleByCategory(cat)

	fmt.Println(result.Result.([]model.Article))
}

func initInMemory() repository.ArticleRepository {
	storage := storage.CreateArticleStorage()

	return inmemory.NewArticleRepositoryInMemory(storage)
}

func initSQLite() repository.ArticleRepository {
	path := "database/sqlite/app.sqlite"
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		panic(err)
	}

	// Run DDL
	ddl, err := ioutil.ReadFile("database/sqlite/ddl.sql")
	if err != nil {
		panic(err)
	}

	sql := string(ddl)

	_, err = db.Exec(sql)
	if err != nil {
		panic(err)
	}

	return sqlite.NewArticleRepositorySQLite(db)
}
