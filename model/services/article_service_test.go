package services

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/fannyhasbi/article-service/model"

	"gitlab.com/fannyhasbi/article-service/repository"
)

func TestGetPublishArticleBySlug(t *testing.T) {
	t.Run("got no article, when there is no published article", func(t *testing.T) {
		repo := repository.CreateArticleStorage()
		slug := "article-tugas-satu"
		article := model.Article{
			Name:     "article tugas satu",
			Body:     "body article tugas satu",
			Category: []model.Category{model.Entertainment},
			Slug:     slug,
			Status:   model.ArticleNotPublished,
		}
		_ = repo.SaveArticle(&article)
		service := NewArticleService(repo)
		got, err := service.GetPublishedArticleBySlug(slug)
		want := model.Article{}
		assert.Error(t, err)
		assert.Equal(t, want, got)
	})

	t.Run("got no article, when the storage is empty", func(t *testing.T) {
		repo := repository.CreateArticleStorage()
		slug := "article-tugas-satu"
		service := NewArticleService(repo)
		got, err := service.GetPublishedArticleBySlug(slug)
		assert.Error(t, err)

		want := model.Article{}
		assert.Equal(t, want, got)
	})

	t.Run("got an article, when there is published articles", func(t *testing.T) {
		repo := repository.CreateArticleStorage()
		slug := "article-tugas-satu"
		article := model.Article{
			Name:     "article tugas satu",
			Body:     "body article tugas satu",
			Category: []model.Category{model.Entertainment},
			Slug:     slug,
			Status:   model.ArticlePublished,
		}
		_ = repo.SaveArticle(&article)
		service := NewArticleService(repo)
		got, err := service.GetPublishedArticleBySlug(slug)
		want := article
		assert.NoError(t, err)
		assert.Equal(t, want, got)
	})
}
