package inmemory

import (
	"errors"

	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
	"gitlab.com/fannyhasbi/article-service/storage"
)

type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory create a storage instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{
		Storage: storage,
	}
}

// Save create new value into map
func (repo *ArticleRepositoryInMemory) Save(article *model.Article) error {
	repo.Storage.ArticleMap[article.Slug] = *article
	return nil
}

// GetPublishedArticleByCategory return published articles filter by category
func (repo *ArticleRepositoryInMemory) GetPublishedArticleByCategory(category model.Category) repository.QueryResult {
	result := repository.QueryResult{}

	var articles []model.Article
	for _, article := range repo.Storage.ArticleMap {
		if article.Status == model.ArticlePublished && article.IsContainsCategory(category) {
			articles = append(articles, article)
		}
	}

	result.Result = articles
	return result
}

// FindBySlug find an article by its slug
func (repo *ArticleRepositoryInMemory) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}

	article, ok := repo.Storage.ArticleMap[slug]
	if !ok {
		result.Result = nil
		result.Error = errors.New("Cannot find slug")
		return result
	}

	result.Result = article
	return result
}
