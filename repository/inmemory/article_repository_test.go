package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/storage"
)

func TestCanFindBySlug(t *testing.T) {
	// given
	article1 := model.Article{
		Name: "Juragan Bakso",
		Slug: "juragan-bakso",
		Body: "Baksooooo",
	}

	article2 := model.Article{
		Name: "FAQ",
		Slug: "faq",
		Body: "Frequently Asked Questions",
	}

	storage := storage.ArticleStorage{
		ArticleMap: map[string]model.Article{
			article1.Slug: article1,
			article2.Slug: article2,
		},
	}

	repo := NewArticleRepositoryInMemory(&storage)

	// when
	result := repo.FindBySlug("juragan-bakso")

	// then
	assert.Equal(t, article1.Slug, result.Result.(model.Article).Slug)
}

// func TestGetPublishedArticleByCategory(t *testing.T) {
// 	// given
// 	articleService := &ArticleServiceSuccess{}
// 	article1, _ := model.CreateArticle(articleService, "Juragan Bakso", "juragan-bakso", "Baksooooo", model.News)
// 	article2, _ := model.CreateArticle(articleService, "FAQ", "faq", "Frequently Asked Questions", model.Politic)

// 	repo := CreateArticleStorage()

// 	// when
// 	repo.SaveArticle(&article1)
// 	repo.SaveArticle(&article2)

// 	articles, err := repo.GetPublishedArticleByCategory(model.News)

// 	// then
// 	assert.Nil(t, err)
// 	assert.Equal(t, 0, len(articles))
// }
