package inmemory

import (
	"gitlab.com/fannyhasbi/article-service/model"
)

const PageLimit = 3

type InMemoryPageStorage struct {
	PageMap map[string]model.Page
}

// CreatePageStorage create a storage for page
func CreateInMemoryPageStorage() *InMemoryPageStorage {
	return &InMemoryPageStorage{
		PageMap: make(map[string]model.Page),
	}
}

// SavePage create a key and value into StorageMap
func (ps *InMemoryPageStorage) SavePage(page *model.Page) string {
	ps.PageMap[page.Slug] = *page
	return page.Slug
}

// IsPageLimited check whether the page is more than the limit
func (ps *InMemoryPageStorage) IsPageLimited() bool {
	if len(ps.PageMap) >= PageLimit {
		return true
	}

	return false
}

func (ps *InMemoryPageStorage) GetAllPages() (result []model.Page) {
	for _, page := range ps.PageMap {
		result = append(result, page)
	}

	return
}

func (s *InMemoryPageStorage) IsSlugExist(slug string) bool {
	for _, post := range s.PageMap {
		if post.Slug == slug {
			return true
		}
	}
	return false
}

type PageStorageSuccess struct{}

func (ps PageStorageSuccess) IsPageLimited() bool {
	return false
}

func (ps PageStorageSuccess) CheckSlugIsExist(slug string) bool {
	return false
}

func (ps PageStorageSuccess) GetAllPages() []model.Page {
	return []model.Page{}
}
