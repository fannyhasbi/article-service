package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/article-service/model"
)

type PageServiceSuccess struct{}

func (ps PageServiceSuccess) IsPageLimited() bool {
	return true
}

func (ps PageServiceSuccess) CheckSlugIsExist(slug string) bool {
	return false
}

func (ps PageServiceSuccess) GetAllPages() []model.Page {
	return []model.Page{}
}

func TestCanSavePageIntoStorage(t *testing.T) {
	// given
	storage := CreateInMemoryPageStorage()
	pageService := PageServiceSuccess{}
	page1, _ := model.CreatePage(pageService, "About Us", "about-us", "This is about us page")
	page2, _ := model.CreatePage(pageService, "FAQ", "faq", "Frequently Asked Questions")

	// when
	storage.SavePage(page1)
	storage.SavePage(page2)

	// then
	assert.Equal(t, storage.PageMap, map[string]model.Page{
		page1.Slug: *page1,
		page2.Slug: *page2,
	})
}

func TestCanGetAllPageFromStorage(t *testing.T) {
	// given
	storage := CreateInMemoryPageStorage()
	pageService := PageServiceSuccess{}
	insertedPage, _ := model.CreatePage(pageService, "About Us", "about-us", "This is about us page")
	expectedPages := []model.Page{*insertedPage}

	// when
	storage.SavePage(insertedPage)
	pages := storage.GetAllPages()

	// then
	assert.Equal(t, pages, expectedPages)
}
