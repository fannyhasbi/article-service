package repository

import "gitlab.com/fannyhasbi/article-service/model"

// QueryResult is wrapper query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleRepository is wrap article repository
type ArticleRepository interface {
	Save(article *model.Article) error
	GetPublishedArticleByCategory(category model.Category) QueryResult
	FindBySlug(slug string) QueryResult
}
