package sqlite

import (
	"database/sql"
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

// ArticleRepositorySQLite store SQLite repo
type ArticleRepositorySQLite struct {
	DB *sql.DB
}

// NewArticleRepositorySQLite create an SQLite storage instance
func NewArticleRepositorySQLite(db *sql.DB) repository.ArticleRepository {
	return &ArticleRepositorySQLite{
		DB: db,
	}
}

func (repo *ArticleRepositorySQLite) Save(article *model.Article) error {
	statement, err := repo.DB.Prepare(`INSERT INTO ARTICLES(SLUG, NAME, BODY) VALUES(?,?,?)`)
	if err != nil {
		return err
	}

	statement.Exec(article.Slug, article.Name, article.Body)

	return nil
}

func (repo *ArticleRepositorySQLite) GetPublishedArticleByCategory(category model.Category) repository.QueryResult {
	result := repository.QueryResult{}
	articles := []model.Article{}

	rows, err := repo.DB.Query(`SELECT SLUG, NAME, BODY FROM ARTICLES`)
	if err != nil {
		result.Error = err
	}

	for rows.Next() {
		article := model.Article{}
		rows.Scan(
			&article.Slug,
			&article.Name,
			&article.Body,
		)

		articles = append(articles, article)
	}

	result.Result = articles
	return result
}

func (repo *ArticleRepositorySQLite) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}
	article := model.Article{}

	row := repo.DB.QueryRow(`SELECT * FROM ARTICLES WHERE SLUG = ?`, slug)
	row.Scan(
		&article.Slug,
		&article.Name,
		&article.Body,
	)

	result.Result = article

	return result
}
